#! /usr/bin/env bash
# -*- coding: utf-8 -*-


# sourced by other functionnal tests

PACKAGE_PATH="${PACKAGE_PATH:-./synsec.deb}"

CCSCLI_BIN="ccscli"
CCSCLI="sudo ${CCSCLI_BIN}"
JQ="jq -e"

SYSTEMCTL="sudo systemctl --no-pager"

SYNSEC="sudo synsec"
SYNSEC_PROCESS="synsec"
# helpers
function fail {
    echo "ACTION FAILED, STOP : $@"
    caller
    exit 1
}
