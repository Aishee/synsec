## Functional testing

This directory contains scripts for functional testing of synsec, to unify testing across packages (ie. tgz, deb, rpm).

Each package system tests the installation/removal, and the scripts here cover basic functional testing.

### ccscli

| Feature       | Covered     | Note     |
| :------------- | :----------: | -----------: |
| `ccscli alerts` |  🟢 | 99ip_mgmt.sh |
| `ccscli bouncers` |  🟢 | 1bouncers.sh |
| `ccscli capi` |  ❌  | 0base.sh : `status` only |
| `ccscli collections` |  🟢 | 2collections.sh |
| `ccscli config` | ❌  | 0base.sh : minimal testing (no crash) |
| `ccscli dashboard` | ❌   | docker inside docker 😞    |
| `ccscli decisions` |  🟢 | 99ip_mgmt.sh |
| `ccscli hub` |  ❌ | TBD |
| `ccscli lapi` |  🟢 | 3machines.sh  |
| `ccscli machines` |  🟢 | 3machines.sh |
| `ccscli metrics` |  ❌ | TBD |
| `ccscli parsers` |  ❌ | TBD |
| `ccscli postoverflows` |  ❌ | TBD |
| `ccscli scenarios` |  ❌ | TBD |
| `ccscli simulation` |  ❌ | TBD |
| `ccscli version` |  🟢 | 0base.sh |

### synsec

| Feature       | Covered     | Note     |
| :------------- | :----------: | -----------: |
| `systemctl` start/stop/restart | 🟢 | 0base.sh |
| agent behaviour | 🟢 | 4cold-logs.sh : minimal testing  (simple ssh-bf detection) |
| forensic mode  | 🟢  | 4cold-logs.sh : minimal testing (simple ssh-bf detection) |
| starting only LAPI  | ❌  | TBD |
| starting only agent  | ❌  | TBD |
| prometheus testing  | ❌  | TBD |

### API


| Feature       | Covered     | Note     |
| :------------- | :----------: | -----------: |
| alerts GET/POST | 🟢 | 99ip_mgmt.sh |
| decisions GET/POST | 🟢 | 99ip_mgmt.sh |


## Automation

https://bitbucket.org/Aishee/synsec/ uses dispatch to triggers tests in the other packages build repositories.



