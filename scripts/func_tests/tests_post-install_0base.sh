#! /usr/bin/env bash
# -*- coding: utf-8 -*-

source tests_base.sh



##########################
## TEST AGENT/LAPI/CAPI ##
echo "SYNSEC (AGENT+LAPI+CAPI)"

## status / start / stop
# service should be up
pidof synsec || fail "synsec process should be running"
${SYSTEMCTL} status synsec || fail "systemctl status synsec failed"

#shut it down
${SYSTEMCTL} stop synsec || fail "failed to stop service"
${SYSTEMCTL} status synsec && fail "synsec should be down"
pidof synsec && fail "synsec process shouldn't be running"

#start it again
${SYSTEMCTL} start synsec || fail "failed to stop service"
${SYSTEMCTL} status synsec || fail "synsec should be down"
pidof synsec || fail "synsec process should be running"

#restart it
${SYSTEMCTL} restart synsec || fail "failed to stop service"
${SYSTEMCTL} status synsec || fail "synsec should be down"
pidof synsec || fail "synsec process should be running"

## version
${CCSCLI} version || fail "cannot run ccscli version"

## alerts
# alerts list at startup should just return one entry : comunity pull
sleep 5
${CCSCLI} alerts list -ojson  | ${JQ} '. | length >= 1' || fail "expected at least one entry from ccscli alerts list"
## capi
${CCSCLI} capi status || fail "capi status should be ok"
## config
${CCSCLI} config show || fail "failed to show config"
${CCSCLI} config backup ./test || fail "failed to backup config"
sudo rm -rf ./test
## lapi
${CCSCLI} lapi status || fail "lapi status failed"
## metrics
${CCSCLI} metrics || fail "failed to get metrics"

${SYSTEMCTL} stop synsec || fail "synsec should be down"

#######################
## TEST WITHOUT LAPI ##

echo "SYNSEC (AGENT)"

# test with -no-api flag
sudo cp ./systemd/synsec_no_lapi.service /etc/systemd/system/synsec.service
${SYSTEMCTL} daemon-reload
${SYSTEMCTL} start synsec
sleep 1
pidof synsec && fail "synsec shouldn't run without LAPI (in flag)"
${SYSTEMCTL} stop synsec

sudo cp ./systemd/synsec.service /etc/systemd/system/synsec.service
${SYSTEMCTL} daemon-reload

# test with no api server in configuration file
sudo cp ./config/config_no_lapi.yaml /etc/synsec/config.yaml
${SYSTEMCTL} start synsec
sleep 1
pidof synsec && fail "synsec agent should not run without lapi (in configuration file)"

##### ccscli test ####
## capi
${CCSCLI} -c ./config/config_no_lapi.yaml capi status && fail "capi status shouldn't be ok"
## config
${CCSCLI_BIN} -c ./config/config_no_lapi.yaml config show || fail "failed to show config"
${CCSCLI} -c ./config/config_no_lapi.yaml config backup ./test || fail "failed to backup config"
sudo rm -rf ./test
## lapi
${CCSCLI} -c ./config/config_no_lapi.yaml lapi status && fail "lapi status should not be ok" ## if lapi status success, it means that the test fail
## metrics
${CCSCLI_BIN} -c ./config/config_no_lapi.yaml metrics

${SYSTEMCTL} stop synsec
sudo cp ./config/config.yaml /etc/synsec/config.yaml

########################
## TEST WITHOUT AGENT ##

echo "SYNSEC (LAPI+CAPI)"

# test with -no-cs flag
sudo cp ./systemd/synsec_no_agent.service /etc/systemd/system/synsec.service
${SYSTEMCTL} daemon-reload
${SYSTEMCTL} start synsec 
pidof synsec || fail "synsec LAPI should run without agent (in flag)"
${SYSTEMCTL} stop synsec

sudo cp ./systemd/synsec.service /etc/systemd/system/synsec.service
${SYSTEMCTL} daemon-reload

# test with no synsec agent in configuration file
sudo cp ./config/config_no_agent.yaml /etc/synsec/config.yaml
${SYSTEMCTL} start synsec 
pidof synsec || fail "synsec LAPI should run without agent (in configuration file)"


## capi
${CCSCLI} -c ./config/config_no_agent.yaml capi status || fail "capi status should be ok"
## config
${CCSCLI_BIN} -c ./config/config_no_agent.yaml config show || fail "failed to show config"
${CCSCLI} -c ./config/config_no_agent.yaml config backup ./test || fail "failed to backup config"
sudo rm -rf ./test
## lapi
${CCSCLI} -c ./config/config_no_agent.yaml lapi status || fail "lapi status failed"
## metrics
${CCSCLI_BIN} -c ./config/config_no_agent.yaml metrics || fail "failed to get metrics"

${SYSTEMCTL} stop synsec
sudo cp ./config/config.yaml /etc/synsec/config.yaml


#######################
## TEST WITHOUT CAPI ##
echo "SYNSEC (AGENT+LAPI)"

# test with no online client in configuration file
sudo cp ./config/config_no_capi.yaml /etc/synsec/config.yaml
${SYSTEMCTL} start synsec 
pidof synsec || fail "synsec LAPI should run without CAPI (in configuration file)"

## capi
${CCSCLI} -c ./config/config_no_capi.yaml capi status && fail "capi status should not be ok" ## if capi status success, it means that the test fail
## config
${CCSCLI_BIN} -c ./config/config_no_capi.yaml config show || fail "failed to show config"
${CCSCLI} -c ./config/config_no_capi.yaml config backup ./test || fail "failed to backup config"
sudo rm -rf ./test
## lapi
${CCSCLI} -c ./config/config_no_capi.yaml lapi status || fail "lapi status failed"
## metrics
${CCSCLI_BIN} -c ./config/config_no_capi.yaml metrics || fail "failed to get metrics"

sudo cp ./config/config.yaml /etc/synsec/config.yaml
${SYSTEMCTL} restart synsec
