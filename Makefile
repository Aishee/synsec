ROOT= $(shell git rev-parse --show-toplevel)
SYSTEM = $(shell uname -s | tr '[A-Z]' '[a-z]')

ifneq ("$(wildcard $(ROOT)/platform/$(SYSTEM).mk)", "")
	include $(ROOT)/platform/$(SYSTEM).mk
else
	include $(ROOT)/platform/linux.mk
endif

PREFIX?="/tmp/synsec/"
CFG_PREFIX = $(PREFIX)"/etc/synsec/"
BIN_PREFIX = $(PREFIX)"/usr/local/bin/"
DATA_PREFIX = $(PREFIX)"/var/run/synsec/"

PID_DIR = $(PREFIX)"/var/run/"
SYNSEC_FOLDER = "./cmd/synsec"
CCSCLI_FOLDER = "./cmd/synsec-cli/"
SYNSEC_BIN = "synsec"
CCSCLI_BIN = "ccscli"
BUILD_CMD = "build"

GOARCH=amd64

#Golang version info
GO_MAJOR_VERSION = $(shell go version | cut -c 14- | cut -d' ' -f1 | cut -d'.' -f1)
GO_MINOR_VERSION = $(shell go version | cut -c 14- | cut -d' ' -f1 | cut -d'.' -f2)
MINIMUM_SUPPORTED_GO_MAJOR_VERSION = 1
MINIMUM_SUPPORTED_GO_MINOR_VERSION = 13
GO_VERSION_VALIDATION_ERR_MSG = Golang version ($(BUILD_GOVERSION)) is not supported, please use least $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION).$(MINIMUM_SUPPORTED_GO_MINOR_VERSION)
#Current versioning information from env
BUILD_VERSION?="$(shell git describe --tags `git rev-list --tags --max-count=1`)"
BUILD_GOVERSION="$(shell go version | cut -d " " -f3 | sed -r 's/[go]+//g')"
BUILD_CODENAME=$(shell cat RELEASE.json | jq -r .CodeName)
BUILD_TIMESTAMP=$(shell date +%F"_"%T)
BUILD_TAG="$(shell git rev-parse HEAD)"
export LD_OPTS=-ldflags "-s -w -X bitbucket.org/Aishee/synsec/pkg/cwversion.Version=$(BUILD_VERSION) \
-X bitbucket.org/Aishee/synsec/pkg/cwversion.BuildDate=$(BUILD_TIMESTAMP) \
-X bitbucket.org/Aishee/synsec/pkg/cwversion.Codename=$(BUILD_CODENAME)  \
-X bitbucket.org/Aishee/synsec/pkg/cwversion.Tag=$(BUILD_TAG) \
-X bitbucket.org/Aishee/synsec/pkg/cwversion.GoVersion=$(BUILD_GOVERSION)"
RELDIR = synsec-$(BUILD_VERSION)

all: clean test build

build: goversion synsec ccscli

static: goversion synsec_static ccscli_static

goversion:
	@if [ $(GO_MAJOR_VERSION) -gt $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION) ]; then \
        exit 0 ;\
    elif [ $(GO_MAJOR_VERSION) -lt $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION) ]; then \
        echo '$(GO_VERSION_VALIDATION_ERR_MSG)';\
        exit 1; \
    elif [ $(GO_MINOR_VERSION) -lt $(MINIMUM_SUPPORTED_GO_MINOR_VERSION) ] ; then \
        echo '$(GO_VERSION_VALIDATION_ERR_MSG)';\
        exit 1; \
    fi

clean:
	@$(MAKE) -C $(SYNSEC_FOLDER) clean --no-print-directory
	@$(MAKE) -C $(CCSCLI_FOLDER) clean --no-print-directory
	@rm -f $(SYNSEC_BIN)
	@rm -f $(CCSCLI_BIN)
	@rm -f *.log
	@rm -f synsec-release.tgz

ccscli:
ifeq ($(lastword $(RESPECT_VERSION)), $(CURRENT_GOVERSION))
	@$(MAKE) -C $(CCSCLI_FOLDER) build --no-print-directory
else
	@echo "Required golang version is $(REQUIRE_GOVERSION). The current one is $(CURRENT_GOVERSION). Exiting.."
	@exit 1;
endif


synsec:
ifeq ($(lastword $(RESPECT_VERSION)), $(CURRENT_GOVERSION))
	@$(MAKE) -C $(SYNSEC_FOLDER) build --no-print-directory
else
	@echo "Required golang version is $(REQUIRE_GOVERSION). The current one is $(CURRENT_GOVERSION). Exiting.."
	@exit 1;
endif


ccscli_static:
ifeq ($(lastword $(RESPECT_VERSION)), $(CURRENT_GOVERSION))
	@$(MAKE) -C $(CCSCLI_FOLDER) static --no-print-directory
else
	@echo "Required golang version is $(REQUIRE_GOVERSION). The current one is $(CURRENT_GOVERSION). Exiting.."
	@exit 1;
endif


synsec_static:
ifeq ($(lastword $(RESPECT_VERSION)), $(CURRENT_GOVERSION))
	@$(MAKE) -C $(SYNSEC_FOLDER) static --no-print-directory
else
	@echo "Required golang version is $(REQUIRE_GOVERSION). The current one is $(CURRENT_GOVERSION). Exiting.."
	@exit 1;
endif

#.PHONY: test
test:
ifeq ($(lastword $(RESPECT_VERSION)), $(CURRENT_GOVERSION))
	@$(MAKE) -C $(SYNSEC_FOLDER) test --no-print-directory
else
	@echo "Required golang version is $(REQUIRE_GOVERSION). The current one is $(CURRENT_GOVERSION). Exiting.."
	@exit 1;
endif

.PHONY: check_release
check_release:
	@if [ -d $(RELDIR) ]; then echo "$(RELDIR) already exists, abort" ;  exit 1 ; fi

.PHONY:
release: check_release build
	@echo Building Release to dir $(RELDIR)
	@mkdir -p $(RELDIR)/cmd/synsec
	@mkdir -p $(RELDIR)/cmd/synsec-cli
	@cp $(SYNSEC_FOLDER)/$(SYNSEC_BIN) $(RELDIR)/cmd/synsec
	@cp $(CCSCLI_FOLDER)/$(CCSCLI_BIN) $(RELDIR)/cmd/synsec-cli
	@cp -R ./config/ $(RELDIR)
	@cp wizard.sh $(RELDIR)
	@cp scripts/test_env.sh $(RELDIR)
	@tar cvzf synsec-release.tgz $(RELDIR)
