ARG GOVERSION=1.14

FROM golang:${GOVERSION}-alpine AS build

WORKDIR /go/src/synsec

RUN apk add --no-cache git jq gcc libc-dev make bash gettext

COPY . .

RUN BUILD_VERSION="$(git describe --tags `git rev-list --tags --max-count=1`)-docker" make release
RUN /bin/bash wizard.sh --docker-mode
RUN ccscli hub update && ccscli collections install breakteam/linux

FROM alpine:latest
RUN wget https://github.com/mikefarah/yq/releases/download/v4.4.1/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
COPY --from=build /etc/synsec /etc/synsec
COPY --from=build /var/lib/synsec /var/lib/synsec
COPY --from=build /usr/local/bin/synsec /usr/local/bin/synsec
COPY --from=build /usr/local/bin/ccscli /usr/local/bin/ccscli
COPY --from=build /go/src/synsec/docker/docker_start.sh /
COPY --from=build /go/src/synsec/docker/config.yaml /etc/synsec/config.yaml

ENTRYPOINT /bin/sh docker_start.sh