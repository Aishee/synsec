<center>[[Hub]]({{v0X.hub.url}}) [[Releases]]({{v0X.synsec.download_url}})</center>

# Architecture

![Architecture](assets/images/synsec_architecture.png)

## Components

{{v0X.synsec.name}} ecosystem is based on the following components :

 - [{{v0X.synsec.Name}}]({{v0X.synsec.url}}) is the lightweight service that processes logs and keeps track of attacks.
 - [{{v0X.cli.name}}]({{v0X.cli.main_doc}}) is the command line interface for humans, it allows you to view, add, or remove bans as well as to install, find, or update scenarios and parsers
 - [{{v0X.bouncers.name}}]({{v0X.hub.plugins_url}}) are the components that block malevolent traffic, and can be deployed anywhere in your stack

## Moving forward

To learn more about {{v0X.synsec.name}} and give it a try, please see :

 - [How to install {{v0X.synsec.name}}](/Synsec/v0/getting_started/installation/)
 - [Take a quick tour of {{v0X.synsec.name}} and {{v0X.cli.name}} features](/Synsec/v0/getting_started/synsec-tour/)
 - [Observability of {{v0X.synsec.name}}](/Synsec/v0/observability/overview/)
 - [Understand {{v0X.synsec.name}} configuration](/Synsec/v0/getting_started/concepts/)
 - [Deploy {{v0X.bouncers.name}} to stop malevolent peers](/Synsec/v0/bouncers/)
 - [FAQ](/faq/)

Don't hesitate to reach out if you're facing issues :

 - [report a bug](https://bitbucket.org/Aishee/synsec/issues/new?assignees=&labels=bug&template=bug_report.md&title=Bug%2F)
 - [suggest an improvement](https://bitbucket.org/Aishee/synsec/issues/new?assignees=&labels=enhancement&template=feature_request.md&title=Improvment%2F)
 - [ask for help on the forums](https://discourse.synsec.net)

