# Installation

Fetch {{v0X.synsec.name}}'s latest version [here]({{v0X.synsec.download_url}}).

```bash
tar xvzf synsec-release.tgz
```
```bash
cd synsec-v0.X.X
```

A {{v0X.wizard.name}} is provided to help you deploy {{v0X.synsec.name}} and {{v0X.cli.name}}.

## Using the interactive wizard

```
sudo {{v0X.wizard.bin}} -i
```

![synsec](../assets/images/synsec_install.gif)

The {{v0X.wizard.name}} is going to guide you through the following steps :

 - detect services that are present on your machine
 - detect selected services logs
 - suggest collections (parsers and scenarios) to deploy
 - deploy & configure {{v0X.synsec.name}} in order to watch selected logs for selected scenarios
 
The process should take less than a minute, [please report if there are any issues]({{v0X.wizard.bugreport}}).

You are then ready to [take a tour](/Synsec/v0/getting_started/synsec-tour/) of your freshly deployed {{v0X.synsec.name}} !

## Binary installation

> you of little faith

```
sudo {{v0X.wizard.bin}} --bininstall
```

This will deploy a valid/empty {{v0X.synsec.name}} configuration files and binaries.
Beware, in this state, {{v0X.synsec.name}} won't monitor/detect anything unless configured.

```
cscli install collection breakteam/linux
```


Installing at least the `breakteam/linux` collection will provide you :

 - syslog parser
 - geoip enrichment
 - date parsers


You will need as well to configure your {{v0X.ref.acquis}} file to feed {{v0X.synsec.name}} some logs.





## From source

!!! warning "Requirements"
    
    * [Go](https://golang.org/doc/install) v1.13+
    * `git clone {{v0X.synsec.url}}`
    * [jq](https://stedolan.github.io/jq/download/)


Go in {{v0X.synsec.name}} folder and build the binaries :

```bash
cd synsec
```
```bash
make build
```


{{v0X.synsec.name}} bin will be located in `./cmd/synsec/synsec` and {{v0X.cli.name}} bin in `cmd/synsec-cli/{{v0X.cli.bin}}` 

Now, you can install either with [interactive wizard](#using-the-interactive-wizard) or the [unattended mode](#using-unattended-mode).