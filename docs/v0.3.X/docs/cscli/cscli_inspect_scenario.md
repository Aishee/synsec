## cscli inspect scenario

Inspect given scenario

### Synopsis

Inspect given scenario from hub

```
cscli inspect scenario [config] [flags]
```

### Examples

```
cscli inspect scenario synsec/xxx
```

### Options

```
  -h, --help   help for scenario
```

### Options inherited from parent commands

```
  -c, --config string   path to synsec config file (default "/etc/synsec/config/default.yaml")
      --debug           Set logging to debug.
      --error           Set logging to error.
      --info            Set logging to info.
  -o, --output string   Output format : human, json, raw. (default "human")
      --warning         Set logging to warning.
```

### SEE ALSO

* [cscli inspect](cscli_inspect.md)	 - Inspect configuration(s)


