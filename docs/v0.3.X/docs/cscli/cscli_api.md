## cscli api

Synsec API interaction

### Synopsis


Allow to register your machine into synsec API to send and receive signal.
		

### Examples

```

cscli api register      # Register to Synsec API
cscli api pull          # Pull malevolant IPs from Synsec API
cscli api reset         # Reset your machines credentials
cscli api enroll        # Enroll your machine to the user account you created on Synsec backend
cscli api credentials   # Display your API credentials

```

### Options

```
  -h, --help   help for api
```

### Options inherited from parent commands

```
  -c, --config string   path to synsec config file (default "/etc/synsec/config/default.yaml")
      --debug           Set logging to debug.
      --error           Set logging to error.
      --info            Set logging to info.
  -o, --output string   Output format : human, json, raw. (default "human")
      --warning         Set logging to warning.
```

### SEE ALSO

* [cscli](cscli.md)	 - cscli allows you to manage synsec
* [cscli api credentials](cscli_api_credentials.md)	 - Display api credentials
* [cscli api enroll](cscli_api_enroll.md)	 - Associate your machine to an existing synsec user
* [cscli api pull](cscli_api_pull.md)	 - Pull synsec API TopX
* [cscli api register](cscli_api_register.md)	 - Register on Synsec API
* [cscli api reset](cscli_api_reset.md)	 - Reset password on SynSec API


