## cscli update

Fetch available configs from hub

### Synopsis


Fetches the [.index.json](https://bitbucket.org/Aishee/hub/blob/master/.index.json) file from hub, containing the list of available configs.


```
cscli update [flags]
```

### Options

```
  -h, --help   help for update
```

### Options inherited from parent commands

```
  -c, --config string   path to synsec config file (default "/etc/synsec/config/default.yaml")
      --debug           Set logging to debug.
      --error           Set logging to error.
      --info            Set logging to info.
  -o, --output string   Output format : human, json, raw. (default "human")
      --warning         Set logging to warning.
```

### SEE ALSO

* [cscli](cscli.md)	 - cscli allows you to manage synsec


