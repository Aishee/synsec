
## Listing installed parsers

{{v0X.parsers.Htmlname}} are yaml files in `{{v0X.config.synsec_dir}}parsers/<STAGE>/parser.yaml`.

!!! info

    Alphabetical file order dictates the order of {{v0X.stage.htmlname}} and the orders of parsers within stage.

You can use the following command to view installed parsers:

```
{{v0X.cli.bin}} list parsers
```

<details>
  <summary>{{v0X.cli.name}} list example</summary>

```bash
# {{v0X.cli.name}} list parsers
INFO[0000] Loaded 9 collecs, 14 parsers, 12 scenarios, 1 post-overflow parsers 
--------------------------------------------------------------------------------------------------------------------
 NAME                       📦 STATUS    VERSION  LOCAL PATH                                                        
--------------------------------------------------------------------------------------------------------------------
 synsec/iptables-logs     ✔️  enabled  0.3      /etc/synsec/config/parsers/s01-parse/iptables-logs.yaml     
 synsec/dateparse-enrich  ✔️  enabled  0.4      /etc/synsec/config/parsers/s02-enrich/dateparse-enrich.yaml 
 synsec/sshd-logs         ✔️  enabled  0.3      /etc/synsec/config/parsers/s01-parse/sshd-logs.yaml         
 synsec/whitelists        ✔️  enabled  0.4      /etc/synsec/config/parsers/s02-enrich/whitelists.yaml       
 synsec/http-logs         ✔️  enabled  0.4      /etc/synsec/config/parsers/s02-enrich/http-logs.yaml        
 synsec/nginx-logs        ✔️  enabled  0.3      /etc/synsec/config/parsers/s01-parse/nginx-logs.yaml        
 synsec/syslog-logs       ✔️  enabled  0.4      /etc/synsec/config/parsers/s00-raw/syslog-logs.yaml         
 synsec/geoip-enrich      ✔️  enabled  0.4      /etc/synsec/config/parsers/s02-enrich/geoip-enrich.yaml     
--------------------------------------------------------------------------------------------------------------------
```

</details>


## Installing parsers

### From the hub

{{v0X.hub.htmlname}} allows you to find needed parsers.

```bash
# {{v0X.cli.name}} install parser synsec/nginx-logs
INFO[0000] Loaded 9 collecs, 14 parsers, 12 scenarios, 1 post-overflow parsers 
INFO[0000] synsec/nginx-logs : OK                     
INFO[0000] Enabled parsers : synsec/nginx-logs        
INFO[0000] Enabled synsec/nginx-logs                  
# systemctl reload synsec
```

### Your own parsers

[Write your parser configuration](/Synsec/v0/write_configurations/parsers/) and deploy yaml file in `{{v0X.config.synsec_dir}}parsers/<STAGE>/`.



## Monitoring parsers behavior

{{v0X.cli.name}} allows you to view {{v0X.synsec.name}} metrics info via the `metrics` command.
This allows you to see how many logs were ingested and then parsed or unparsed by said parser.

You can see those metrics with the following command:
```
cscli metrics
```

<details>
  <summary>{{v0X.cli.name}} metrics example</summary>

```bash
# {{v0X.cli.name}} metrics
...
INFO[0000] Parser Metrics:                              
+---------------------------+--------+--------+----------+
|          PARSERS          |  HITS  | PARSED | UNPARSED |
+---------------------------+--------+--------+----------+
| synsec/sshd-logs        |  62424 |  12922 |    49502 |
| synsec/syslog-logs      | 667417 | 667417 |        0 |
| synsec/whitelists       | 610901 | 610901 |        0 |
| synsec/http-logs        |    136 |     21 |      115 |
| synsec/iptables-logs    | 597843 | 597843 |        0 |
| synsec/nginx-logs       |    137 |    136 |        1 |
| synsec/dateparse-enrich | 610901 | 610901 |        0 |
| synsec/geoip-enrich     | 610836 | 610836 |        0 |
| synsec/non-syslog       |    137 |    137 |        0 |
+---------------------------+--------+--------+----------+

```

</details>


## Going further

If you're interested into [understanding how parsers are made](/Synsec/v0/references/parsers/) or writing your own, please have a look at [this page](/Synsec/v0/write_configurations/parsers/).

