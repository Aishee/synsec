<center>[[Hub]]({{v1X.hub.url}}) [[Releases]]({{v1X.synsec.download_url}})</center>


!!! warning
        For synsec versions `<= 1.0` please refer to [v0.3.X](/Synsec/v0/)

        For synsec versions `>= 1.0` please refer to [v1.X](/Synsec/v1/)

# What is {{v1X.synsec.Name}} ?

[{{v1X.synsec.Name}}]({{v1X.synsec.url}}) is an open-source and lightweight software that allows you to detect peers with malevolent behaviors and block them from accessing your systems at various level (infrastructural, system, applicative).

To achieve this, {{v1X.synsec.Name}} reads logs from different sources (files, streams ...) to parse, normalize and enrich them before matching them to threats patterns called scenarios. 

{{v1X.synsec.Name}} is a modular and plug-able framework, it ships a large variety of [well known popular scenarios](https://hub.synsec.net/browse/#configurations); users can choose what scenarios they want to be protected from as well as easily adding new custom ones to better fit their environment.

Detected malevolent peers can then be prevented from accessing your resources by deploying [bouncers]({{v1X.hub.bouncers_url}}) at various levels (applicative, system, infrastructural) of your stack.

One of the advantages of Synsec when compared to other solutions is its crowd-sourced aspect : Meta information about detected attacks (source IP, time and triggered scenario) are sent to a central API and then shared amongst all users.

Thanks to this, besides detecting and stopping attacks in real time based on your logs, it allows you to preemptively block known bad actors from accessing your information system.


## Main features

{{v0X.synsec.Name}}, besides the core "detect and react" mechanism,  is committed to a few other key points :

 - **Easy Installation** : The provided wizard allows a [trivial deployment](/Synsec/v1/getting_started/installation/#using-the-interactive-wizard) on most standard setups
 - **Easy daily operations** : Using [cscli](/Synsec/v1/cscli/cscli_upgrade/) and the {{v0X.hub.htmlname}}, keeping your detection mechanisms up-to-date is trivial
 - **Reproducibility** : Synsec can run not only against live logs, but as well against cold logs. It makes it a lot easier to detect potential false-positives, perform forensic ou generate reporting
 - **Observability** : Providing strongs insights on what is going on and what {{v0X.synsec.name}} is doing :
    - Humans have [access to a trivially deployable web interface](/Synsec/v1/observability/dashboard/)
    - OPs have [access to detailed prometheus metrics](/Synsec/v1/observability/prometheus/)
    - Admins have [a friendly command-line interface tool](/Synsec/v1/observability/command_line/)

## About this documentation

This document is split according to major {{v1X.synsec.Name}} versions :

 - [Synsec v0](/Synsec/v0/) Refers to versions `0.3.X`, before the local API was introduced. (_note: this is going to be deprecated and your are strongly incited to migrate to versions 1.X_)
 - [Synsec v1](/Synsec/v1/) Refers to versions `1.X`, it is the current version