<center>[[Hub]]({{v1X.hub.url}}) [[Releases]]({{v1X.synsec.download_url}})</center>

# Architecture

![Architecture](assets/images/synsec_architecture.png)


## Components

{{v1X.synsec.name}} ecosystem is based on the following components :

 - [{{v1X.synsec.Name}}]({{v1X.synsec.url}}) is the lightweight service that processes logs and keeps track of attacks.
 - [{{v1X.lapi.Name}}]({{v1X.lapi.url}}) is a core component of synsec-agent that exposes a local API to interact with synsec-agent.
 - [{{v1X.cli.name}}]({{v1X.cli.main_doc}}) is the command line interface for humans, it allows you to view, add, or remove bans as well as to install, find, or update scenarios and parsers
 - [{{v1X.bouncers.name}}]({{v1X.hub.bouncers_url}}) are the components that block malevolent traffic, and can be deployed anywhere in your stack

## Moving forward

To learn more about {{v1X.synsec.name}} and give it a try, please see :

 - [How to install {{v1X.synsec.name}}](/Synsec/v1/getting_started/installation/)
 - [Take a quick tour of {{v1X.synsec.name}} and {{v1X.cli.name}} features](/Synsec/v1/getting_started/synsec-tour/)
 - [Observability of {{v1X.synsec.name}}](/Synsec/v1/observability/overview/)
 - [Understand {{v1X.synsec.name}} configuration](/Synsec/v1/getting_started/concepts/)
 - [Deploy {{v1X.bouncers.name}} to stop malevolent peers](/Synsec/v1/bouncers/)
 - [FAQ](/faq/)

Don't hesitate to reach out if you're facing issues :

 - [report a bug](https://bitbucket.org/Aishee/synsec/issues/new?assignees=&labels=bug&template=bug_report.md&title=Bug%2F)
 - [suggest an improvement](https://bitbucket.org/Aishee/synsec/issues/new?assignees=&labels=enhancement&template=feature_request.md&title=Improvment%2F)
 - [ask for help on the forums](https://discourse.synsec.net)