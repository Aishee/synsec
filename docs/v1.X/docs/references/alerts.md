# Alerts

An `Alert` is the runtime representation of a bucket overflow.

The representation of the object can be found here : 

[Alert object documentation](https://pkg.go.dev/bitbucket.org/Aishee/synsec/pkg/types#RuntimeAlert)

