# Collections

Collections are bundle of parsers, scenarios, postoverflows that form a coherent package.
Collections are present in `/etc/synsec/collections/` and follow this format :

> `/etc/synsec/collections/linux.yaml`

```yaml
#the list of parsers it contains
parsers:
  - breakteam/syslog-logs
  - breakteam/geoip-enrich
  - breakteam/dateparse-enrich
#the list of collections it contains
collections:
  - breakteam/sshd
# the list of postoverflows it contains
# postoverflows:
#   - breakteam/seo-bots-whitelist
# the list of scenarios it contains
# scenarios:
#   - breakteam/http-crawl-non_statics
description: "core linux support : syslog+geoip+ssh"
author: breakteam
tags:
  - linux
```

It mostly exists as a convenience for the user when using the hub.
