# Requirements

> - Having read and understood [`synsec` concepts](/Synsec/v1/getting_started/concepts/)

> - Some requirements are needed in order to be able to write your own end-to-end configurations.

> - During all this documentation, we are going to show as an exemple how we wrote a full port scan detection scenario (from acqusition to scenario, including parser)


## Create the test environment

First of all, please [download the latest release of {{v1X.synsec.name}}](https://bitbucket.org/Aishee/synsec/releases).

Then run the following commands:

```bash
tar xzvf synsec-release.tgz
```
```bash
cd ./synsec-vX.Y/
```
```bash
./test_env.sh  # the -o is facultative, default is "./tests/"
```
```bash
cd ./tests/
```

The `./test_env.sh` script creates a local (non privileged) working environement for {{v1X.synsec.name}} and {{v1X.cli.name}}.
The deployed environment is intended to write and test parsers and scenarios easily.


<details>
  <summary>Example</summary>

```bash
$ tar xzvf ./synsec-release.tgz
$ cd ./synsec-v*/
$ ./test_env.sh 
[12/11/2020:11:45:19][INFO] Creating test arboresence in /tmp/synsec-v1.0.0/tests
[12/11/2020:11:45:19][INFO] Arboresence created
[12/11/2020:11:45:19][INFO] Copying needed files for tests environment
[12/11/2020:11:45:19][INFO] Files copied
[12/11/2020:11:45:19][INFO] Setting up configurations
INFO[0000] Machine 'test' created successfully          
INFO[0000] API credentials dumped to '/tmp/synsec-v1.0.0/tests/config/local_api_credentials.yaml' 
INFO[0000] Wrote new 73826 bytes index to /tmp/synsec-v1.0.0/tests/config/hub/.index.json 
INFO[0000] breakteam/syslog-logs : OK               
INFO[0000] breakteam/geoip-enrich : OK              
INFO[0000] downloading data 'https://synsec-statics-assets.s3-eu-west-1.amazonaws.com/GeoLite2-City.mmdb' in '/tmp/synsec-v1.0.0/tests/data/GeoLite2-City.mmdb' 
INFO[0002] downloading data 'https://synsec-statics-assets.s3-eu-west-1.amazonaws.com/GeoLite2-ASN.mmdb' in '/tmp/synsec-v1.0.0/tests/data/GeoLite2-ASN.mmdb' 
INFO[0003] breakteam/dateparse-enrich : OK          
INFO[0003] breakteam/sshd-logs : OK                 
INFO[0004] breakteam/ssh-bf : OK                    
INFO[0004] breakteam/sshd : OK                      
WARN[0004] breakteam/sshd : overwrite               
INFO[0004] breakteam/linux : OK                     
INFO[0004] /tmp/synsec-v1.0.0/tests/config/collections doesn't exist, create 
INFO[0004] Enabled parsers : breakteam/syslog-logs  
INFO[0004] Enabled parsers : breakteam/geoip-enrich 
INFO[0004] Enabled parsers : breakteam/dateparse-enrich 
INFO[0004] Enabled parsers : breakteam/sshd-logs    
INFO[0004] Enabled scenarios : breakteam/ssh-bf     
INFO[0004] Enabled collections : breakteam/sshd     
INFO[0004] Enabled collections : breakteam/linux    
INFO[0004] Enabled breakteam/linux                  
INFO[0004] Run 'systemctl reload synsec' for the new configuration to be effective. 
[12/11/2020:11:45:25][INFO] Environment is ready in /tmp/synsec-v1.0.0/tests

```


