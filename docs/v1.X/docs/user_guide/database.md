# Databases

By default, the synsec Local API use `SQLite` as backend storage. But in case you expect a lot of traffic on your local API, you should use `MySQL` or `PostgreSQL`.

For `SQLite`, there is nothing to do to make it work with synsec. But for `MySQL` and `PostgreSQL` , you have to create the database and the user.

Please refer to [ent.](https://entgo.io/) [supported database](https://entgo.io/docs/dialects/). At the time of writting :

 - MySQL `5.6.35`, `5.7.26` and `8`
 - MariaDB `10.2` and latest
 - PostgreSQL `10`, `11` and `12`
 - SQLite
 - Gremlin


## MySQL

Connect to your `MySQL` server and run the following commands:

```
mysql> CREATE DATABASE synsec;
mysql> CREATE USER 'synsec'@'%' IDENTIFIED BY '<password>';
mysql> GRANT ALL PRIVILEGES ON synsec.* TO 'synsec'@'%';
mysql> FLUSH PRIVILEGES;
```

Then edit `{{v1X.config.synsec_config_file}}` to update the [`db_config`](/Synsec/v1/references/database/#db_config) part.

You can now start/restart synsec.

## PostgreSQL

Connect to your `PostgreSQL` server and run the following commands:

```
postgres=# CREATE DATABASE synsec;
postgres=# CREATE USER synsec WITH PASSWORD '<password>';
postgres=# GRANT ALL PRIVILEGES ON DATABASE synsec TO synsec;
```

Then edit `{{v1X.config.synsec_config_file}}` to update the [`db_config`](/Synsec/v1/references/database/#db_config) part.

You can now start/restart synsec.