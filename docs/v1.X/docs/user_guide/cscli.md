# Overview

`{{v1X.cli.name}}` is the utility that will help you to manage {{v1X.synsec.name}}. This tool has the following functionalities:

 - manage [decisions](/Synsec/v1/cscli/cscli_decisions/) and [alerts](/Synsec/v1/cscli/cscli_alerts/) : This is how you monitor ongoing remediation and detections
 - manage configurations such as [collections](/Synsec/v1/cscli/cscli_collections/), [parsers](/Synsec/v1/cscli/cscli_parsers/), [scenarios](/Synsec/v1/cscli/cscli_scenarios/) : This is how you install/update {{v1X.synsec.htmname}}'s detection capabilities and manage whitelists
 - interact with the [hub](/Synsec/v1/cscli/cscli_hub/) to find new configurations or update existing ones
 - manage local api (LAPI) [bouncers](/Synsec/v1/cscli/cscli_bouncers/) and [machines](/Synsec/v1/cscli/cscli_machines/) : This allows you to manage LAPI credentials, this is how you make {{v1X.synsec.htmname}} and bouncers comunicate
 - observe synsec via [metrics](/Synsec/v1/cscli/cscli_metrics/) or the [dashboard](/Synsec/v1/cscli/cscli_dashboard/) : This is how you gain real-time observability 
 - manage [simulation](/Synsec/v1/cscli/cscli_simulation/) configurations, allowing you to disable/modify remediation triggered by specific scenarios


Take a look at the [dedicated documentation](/Synsec/v1/cscli/cscli)

!!! tips
    You can enable `cscli` auto completion in `bash` or `zsh`.

    You can find `cscli completion` documentation [here](/Synsec/v1/cscli/cscli_completion/).

# Configuration

`{{v1X.cli.name}}` shares the configuration file of {{v1X.synsec.name}}, usually in `/etc/synsec/config.yaml`
