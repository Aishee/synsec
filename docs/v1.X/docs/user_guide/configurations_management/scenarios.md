{{v1X.hub.htmlname}} allows you to find needed scenarios.

## Installing scenarios

```bash
$ sudo cscli scenarios install breakteam/http-bf-wordpress_bf
```

<details>
  <summary>cscli scenarios install example</summary>

```bash
$ sudo cscli scenarios install breakteam/http-bf-wordpress_bf
INFO[0000] breakteam/http-bf-wordpress_bf : OK      
INFO[0000] Enabled scenarios : breakteam/http-bf-wordpress_bf 
INFO[0000] Enabled breakteam/http-bf-wordpress_bf   
INFO[0000] Run 'systemctl reload synsec' for the new configuration to be effective. 
$ systemctl reload synsec
```

</details>


## Listing installed scenarios

```bash
sudo cscli scenarios list
```

{{v1X.scenarios.Htmlname}} are yaml files in `{{v1X.config.synsec_dir}}scenarios/`.


<details>
  <summary>cscli scenarios list example</summary>

```bash
$ sudo cscli scenarios list
---------------------------------------------------------------------------------------------------------------------------
 NAME                                       📦 STATUS    VERSION  LOCAL PATH                                               
---------------------------------------------------------------------------------------------------------------------------
 breakteam/ssh-bf                       ✔️  enabled  0.1      /etc/synsec/scenarios/ssh-bf.yaml                      
 breakteam/http-bf-wordpress_bf         ✔️  enabled  0.1      /etc/synsec/scenarios/http-bf-wordpress_bf.yaml        
 breakteam/http-crawl-non_statics       ✔️  enabled  0.2      /etc/synsec/scenarios/http-crawl-non_statics.yaml      
 breakteam/http-probing                 ✔️  enabled  0.1      /etc/synsec/scenarios/http-probing.yaml                
 breakteam/http-sensitive-files         ✔️  enabled  0.2      /etc/synsec/scenarios/http-sensitive-files.yaml        
 breakteam/http-bad-user-agent          ✔️  enabled  0.2      /etc/synsec/scenarios/http-bad-user-agent.yaml         
 breakteam/http-path-traversal-probing  ✔️  enabled  0.2      /etc/synsec/scenarios/http-path-traversal-probing.yaml 
 breakteam/http-sqli-probing            ✔️  enabled  0.2      /etc/synsec/scenarios/http-sqli-probing.yaml           
 breakteam/http-backdoors-attempts      ✔️  enabled  0.2      /etc/synsec/scenarios/http-backdoors-attempts.yaml     
 breakteam/http-xss-probing             ✔️  enabled  0.2      /etc/synsec/scenarios/http-xss-probing.yaml            
---------------------------------------------------------------------------------------------------------------------------

```

</details>


## Upgrading installed scenarios

```bash
$ sudo cscli scenarios upgrade breakteam/sshd-bf
```

Scenarios upgrade allows you to upgrade an existing scenario to the latest version.

<details>
  <summary>cscli scenarios upgrade example</summary>

```bash
$ sudo cscli scenarios upgrade breakteam/ssh-bf
INFO[0000] breakteam/ssh-bf : up-to-date            
WARN[0000] breakteam/ssh-bf : overwrite             
INFO[0000] 📦 breakteam/ssh-bf : updated             
INFO[0000] Upgraded 1 items                             
INFO[0000] Run 'systemctl reload synsec' for the new configuration to be effective. 
```

</details>

## Monitoring scenarios

```bash
$ sudo cscli scenarios inspect breakteam/ssh-bf
```

Scenarios inspect will give you detailed information about a given scenario, including versioning information *and* runtime metrics (fetched from prometheus).

<details>
  <summary>cscli scenarios inspect example</summary>

```bash
$ sudo cscli scenarios inspect breakteam/ssh-bf    
type: scenarios
name: breakteam/ssh-bf
filename: ssh-bf.yaml
description: Detect ssh bruteforce
author: breakteam
references:
- http://wikipedia.com/ssh-bf-is-bad
belongs_to_collections:
- breakteam/sshd
remote_path: scenarios/breakteam/ssh-bf.yaml
version: "0.1"
local_path: /etc/synsec/scenarios/ssh-bf.yaml
localversion: "0.1"
localhash: 4441dcff07020f6690d998b7101e642359ba405c2abb83565bbbdcee36de280f
installed: true
downloaded: true
uptodate: true
tainted: false
local: false

Current metrics :

 - (Scenario) breakteam/ssh-bf:
+---------------+-----------+--------------+--------+---------+
| CURRENT COUNT | OVERFLOWS | INSTANCIATED | POURED | EXPIRED |
+---------------+-----------+--------------+--------+---------+
|            14 |      5700 |         7987 |  42572 |    2273 |
+---------------+-----------+--------------+--------+---------+
```

<details>

## Reference documentation

[Link to scenarios reference documentation](/Synsec/v1/references/scenarios/)
