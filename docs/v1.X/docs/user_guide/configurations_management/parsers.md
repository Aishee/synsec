{{v1X.hub.htmlname}} allows you to find needed parsers.

## Installing parsers

```bash
$ sudo cscli parsers install breakteam/sshd-logs
```

<details>
  <summary>cscli parsers install example</summary>

```bash
$ sudo cscli parsers install breakteam/iptables-logs    
INFO[0000] breakteam/iptables-logs : OK             
INFO[0000] Enabled parsers : breakteam/iptables-logs 
INFO[0000] Enabled breakteam/iptables-logs          
INFO[0000] Run 'systemctl reload synsec' for the new configuration to be effective. 
```
</details>

## Listing installed parsers

```bash
sudo cscli parsers list
```

{{v1X.parsers.Htmlname}} are yaml files in `{{v1X.config.synsec_dir}}parsers/<STAGE>/parser.yaml`.


<details>
  <summary>cscli parsers list example</summary>

```bash
$ sudo cscli parsers list
--------------------------------------------------------------------------------------------------------------
 NAME                            📦 STATUS    VERSION  LOCAL PATH                                             
--------------------------------------------------------------------------------------------------------------
 breakteam/whitelists        ✔️  enabled  0.1      /etc/synsec/parsers/s02-enrich/whitelists.yaml       
 breakteam/dateparse-enrich  ✔️  enabled  0.1      /etc/synsec/parsers/s02-enrich/dateparse-enrich.yaml 
 breakteam/iptables-logs     ✔️  enabled  0.1      /etc/synsec/parsers/s01-parse/iptables-logs.yaml     
 breakteam/syslog-logs       ✔️  enabled  0.1      /etc/synsec/parsers/s00-raw/syslog-logs.yaml         
 breakteam/sshd-logs         ✔️  enabled  0.1      /etc/synsec/parsers/s01-parse/sshd-logs.yaml         
 breakteam/geoip-enrich      ✔️  enabled  0.2      /etc/synsec/parsers/s02-enrich/geoip-enrich.yaml     
 breakteam/http-logs         ✔️  enabled  0.2      /etc/synsec/parsers/s02-enrich/http-logs.yaml        
 breakteam/nginx-logs        ✔️  enabled  0.1      /etc/synsec/parsers/s01-parse/nginx-logs.yaml        
--------------------------------------------------------------------------------------------------------------

```

</details>


## Upgrading installed parsers

```bash
$ sudo {{v1X.cli.bin}} parsers upgrade breakteam/sshd-logs
```

Parsers upgrade allows you to upgrade an existing parser to the latest version.

<details>
  <summary>cscli parsers upgrade example</summary>

```bash
$ sudo cscli parsers upgrade breakteam/sshd-logs  
INFO[0000] breakteam/sshd : up-to-date              
WARN[0000] breakteam/sshd-logs : overwrite          
WARN[0000] breakteam/ssh-bf : overwrite             
WARN[0000] breakteam/sshd : overwrite               
INFO[0000] 📦 breakteam/sshd : updated               
INFO[0000] Upgraded 1 items                             
INFO[0000] Run 'systemctl reload synsec' for the new configuration to be effective.

```

</details>

## Monitoring parsers

```bash
$ sudo cscli parsers inspect breakteam/sshd-logs 
```

Parsers inspect will give you detailed information about a given parser, including versioning information *and* runtime metrics (fetched from prometheus).

<!--TBD: refaire l'output apres avoir fix le 'parsers inspect XXXX'-->
<details>
  <summary>cscli parsers inspect example</summary>

```bash
$ sudo cscli parsers inspect breakteam/sshd-logs     
type: parsers
stage: s01-parse
name: breakteam/sshd-logs
filename: sshd-logs.yaml
description: Parse openSSH logs
author: breakteam
belongs_to_collections:
- breakteam/sshd
remote_path: parsers/s01-parse/breakteam/sshd-logs.yaml
version: "0.1"
local_path: /etc/synsec/parsers/s01-parse/sshd-logs.yaml
localversion: "0.1"
localhash: ecd40cb8cd95e2bad398824ab67b479362cdbf0e1598b8833e2f537ae3ce2f93
installed: true
downloaded: true
uptodate: true
tainted: false
local: false

Current metrics :

 - (Parser) breakteam/sshd-logs:
+-------------------+-------+--------+----------+
|      PARSERS      | HITS  | PARSED | UNPARSED |
+-------------------+-------+--------+----------+
| /var/log/auth.log | 94138 |  42404 |    51734 |
+-------------------+-------+--------+----------+

```

<details>

## Reference documentation

[Link to parsers reference documentation](/Synsec/v1/references/parsers/)

