
{{v1X.hub.htmlname}} allows you to find needed collections.

## Installing collections

```bash
$ sudo cscli collections install breakteam/whitelist-good-actors
```

<details>
  <summary>{{v1X.cli.name}} collection install example</summary>

```bash
$ sudo cscli collections install breakteam/whitelist-good-actors
INFO[0000] breakteam/seo-bots-whitelist : OK        
INFO[0000] downloading data 'https://raw.githubusercontent.com/breakteam/sec-lists/master/whitelists/benign_bots/search_engine_crawlers/rdns_seo_bots.txt' in '/var/lib/synsec/data/rdns_seo_bots.txt' 
INFO[0001] downloading data 'https://raw.githubusercontent.com/breakteam/sec-lists/master/whitelists/benign_bots/search_engine_crawlers/rnds_seo_bots.regex' in '/var/lib/synsec/data/rdns_seo_bots.regex' 
INFO[0002] downloading data 'https://raw.githubusercontent.com/breakteam/sec-lists/master/whitelists/benign_bots/search_engine_crawlers/ip_seo_bots.txt' in '/var/lib/synsec/data/ip_seo_bots.txt' 
INFO[0002] breakteam/cdn-whitelist : OK             
INFO[0002] downloading data 'https://www.cloudflare.com/ips-v4' in '/var/lib/synsec/data/cloudflare_ips.txt' 
INFO[0003] breakteam/rdns : OK                      
INFO[0003] breakteam/whitelist-good-actors : OK     
INFO[0003] /etc/synsec/postoverflows/s01-whitelist doesn't exist, create 
INFO[0003] Enabled postoverflows : breakteam/seo-bots-whitelist 
INFO[0003] Enabled postoverflows : breakteam/cdn-whitelist 
INFO[0003] /etc/synsec/postoverflows/s00-enrich doesn't exist, create 
INFO[0003] Enabled postoverflows : breakteam/rdns   
INFO[0003] Enabled collections : breakteam/whitelist-good-actors 
INFO[0003] Enabled breakteam/whitelist-good-actors  
INFO[0003] Run 'systemctl reload synsec' for the new configuration to be effective. 
$ systemctl reload synsec
```
</details>


## Listing installed collections

```bash
$ sudo {{v1X.cli.bin}} collections list
```

<details>
  <summary>cscli collections list example</summary>

```bash
$ sudo cscli collections list   
-------------------------------------------------------------------------------------------------------------
 NAME                               📦 STATUS    VERSION  LOCAL PATH                                         
-------------------------------------------------------------------------------------------------------------
 breakteam/nginx                ✔️  enabled  0.1      /etc/synsec/collections/nginx.yaml               
 breakteam/base-http-scenarios  ✔️  enabled  0.1      /etc/synsec/collections/base-http-scenarios.yaml 
 breakteam/sshd                 ✔️  enabled  0.1      /etc/synsec/collections/sshd.yaml                
 breakteam/linux                ✔️  enabled  0.2      /etc/synsec/collections/linux.yaml               
-------------------------------------------------------------------------------------------------------------
```

</details>

## Upgrading installed collections

```bash
$ sudo {{v1X.cli.bin}} hub update
$ sudo {{v1X.cli.bin}} collections upgrade breakteam/sshd
```

Collection upgrade allows you to upgrade an existing collection (and its items) to the latest version.


<details>
  <summary>cscli collections upgrade example</summary>

```bash
$ sudo cscli collections upgrade breakteam/sshd  
INFO[0000] breakteam/sshd : up-to-date              
WARN[0000] breakteam/sshd-logs : overwrite          
WARN[0000] breakteam/ssh-bf : overwrite             
WARN[0000] breakteam/sshd : overwrite               
INFO[0000] 📦 breakteam/sshd : updated               
INFO[0000] Upgraded 1 items                             
INFO[0000] Run 'systemctl reload synsec' for the new configuration to be effective.
$ systemctl reload synsec

```

</details>

## Monitoring collections

```bash
$ sudo cscli collections inspect breakteam/sshd
```

Collections inspect will give you detailed information about a given collection, including versioning information *and* runtime metrics (fetched from prometheus).

<details>
  <summary>cscli collections inspect example</summary>

```bash
$ sudo cscli collections inspect breakteam/sshd       
type: collections
name: breakteam/sshd
filename: sshd.yaml
description: 'sshd support : parser and brute-force detection'
author: breakteam
belongs_to_collections:
- breakteam/linux
- breakteam/linux
remote_path: collections/breakteam/sshd.yaml
version: "0.1"
local_path: /etc/synsec/collections/sshd.yaml
localversion: "0.1"
localhash: 21159aeb87529efcf1a5033f720413d5321a6451bab679a999f7f01a7aa972b3
installed: true
downloaded: true
uptodate: true
tainted: false
local: false
parsers:
- breakteam/sshd-logs
scenarios:
- breakteam/ssh-bf

Current metrics : 

 - (Scenario) breakteam/ssh-bf: 
+---------------+-----------+--------------+--------+---------+
| CURRENT COUNT | OVERFLOWS | INSTANCIATED | POURED | EXPIRED |
+---------------+-----------+--------------+--------+---------+
|             0 |         1 |            2 |     10 |       1 |
+---------------+-----------+--------------+--------+---------+

```

</details>

## Reference documentation

[Link to collections reference documentation](/Synsec/v1/references/collections/)
