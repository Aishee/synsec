## cscli config

Allows to view current config

### Options

```
  -h, --help   help for config
```

### Options inherited from parent commands

```
  -c, --config string   path to synsec config file (default "/etc/synsec/config.yaml")
      --debug           Set logging to debug.
      --error           Set logging to error.
      --info            Set logging to info.
  -o, --output string   Output format : human, json, raw.
      --trace           Set logging to trace.
      --warning         Set logging to warning.
```

### SEE ALSO

* [cscli](cscli.md)	 - cscli allows you to manage synsec
* [cscli config backup](cscli_config_backup.md)	 - Backup current config
* [cscli config restore](cscli_config_restore.md)	 - Restore config in backup <directory>
* [cscli config show](cscli_config_show.md)	 - Displays current config


