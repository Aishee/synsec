# Synsec

Synsec - An open-source, lightweight agent to detect and respond to bad behaviours. It also automatically benefits from our global community-wide IP reputation database.

## Getting Started

Before starting using docker image, we suggest you to read our documentation to understand all [synsec concepts](https://docs.synsec.net/).

#### Run

The container is built with specific docker [configuration](https://bitbucket.org/Aishee/synsec/blob/master/docker/config.yaml) :

You should apply following configuration before starting it :

* Specify collections|scenarios|parsers/postoverflows to install via the environment variables (by default [`breakteam/linux`](https://hub.synsec.net/author/breakteam/collections/linux) is installed)
* Mount volumes to specify your log files that should be ingested by synsec (set up in acquis.yaml)
* Mount other volumes : if you want to share the database for example

```shell
docker run -d -v acquis.yaml:/etc/synsec/acquis.yaml \
    -e COLLECTIONS="breakteam/sshd"
    -v /var/log/auth.log:/var/log/auth.log \
    -v /path/mycustom.log:/var/log/mycustom.log \
    --name synsec breakteam/synsec
```

#### Example

I have my own configuration :
```shell
user@cs ~/synsec/config $ ls
acquis.yaml  config.yaml
```

Here is my acquis.yaml file:
```shell
filenames:
 - /logs/auth.log
 - /logs/syslog
labels:
  type: syslog
---
filename: /logs/apache2/*.log
labels:
  type: apache2
```

So, I want to run synsec with :

* My configuration files
* Ingested my path logs specified in acquis.yaml
* Share the synsec sqlite database with my host (You need to create empty file first, otherwise docker will create a directory instead of simple file)
* Expose local API through host (listen by default on `8080`)
* Expose prometheus handler through host (listen by default on `6060`)

```shell
touch /path/myDatabase.db
docker run -d -v config.yaml:/etc/synsec/config.yaml \
    -v acquis.yaml:/etc/synsec/acquis.yaml \
    -v /var/log/auth.log:/logs/auth.log \
    -v /var/log/syslog.log:/logs/syslog.log \
    -v /var/log/apache:/logs/apache \
    -v /path/myDatabase.db:/var/lib/synsec/data/synsec.db \
    -e COLLECTIONS="breakteam/apache2 breakteam/sshd" \
    -p 8080:8080 -p 6060:6060 \
    --name synsec breakteam/synsec
```

If you want to be able to restart/stop your container and keep the same DB `-v /path/myDatabase.db:/var/lib/synsec/data/synsec.db` you need to add a volume on local_api_credentials.yaml `-v /path/local_api_credentials.yaml:/etc/synsec/local_api_credentials.yaml`.

### Environment Variables

* `COLLECTIONS`             - Collections to install from the [hub](https://hub.synsec.net/browse/#collections), separated by space : `-e COLLECTIONS="breakteam/linux breakteam/apache2"`
* `SCENARIOS`               - Scenarios to install from the [hub](https://hub.synsec.net/browse/#configurations), separated by space : `-e SCENARIOS="breakteam/http-bad-user-agent breakteam/http-xss-probing"`
* `PARSERS`                 - Parsers to install from the [hub](https://hub.synsec.net/browse/#configurations), separated by space : `-e PARSERS="breakteam/http-logs breakteam/modsecurity"`
* `POSTOVERFLOWS`           - Postoverflows to install from the [hub](https://hub.synsec.net/browse/#configurations), separated by space : `-e POSTOVERFLOWS="breakteam/cdn-whitelist"`
* `CONFIG_FILE`             - Configuration file (default: `/etc/synsec/config.yaml`) : `-e CONFIG_FILE="<config_path>"`
* `FILE_PATH`               - Process a single file in time-machine : `-e FILE_PATH="<file_path>"`
* `JOURNALCTL_FILTER`       - Process a single journalctl output in time-machine : `-e JOURNALCTL_FILTER="<journalctl_filter>"`
* `TYPE`                    - [`Labels.type`](https://docs.synsec.net/Synsec/v1/references/acquisition/) for file in time-machine : `-e TYPE="<type>"`
* `TEST_MODE`               - Only test configs (default: `false`) : `-e TEST_MODE="<true|false>"`
* `DISABLE_AGENT`           - Only test configs (default: `false`) : `-e DISABLE_AGENT="<true|false>"`
* `DISABLE_LOCAL_API`       - Disable local API (default: `false`) : `-e DISABLE_API="<true|false>"`
* `DISABLE_ONLINE_API`      - Disable Online API registration for signal sharing (default: `false`) : `-e DISABLE_ONLINE_API="<true|false>"`
* `LEVEL_TRACE`             - Trace-level (VERY verbose) on stdout (default: `false`) : `-e LEVEL_TRACE="<true|false>"`
* `LEVEL_DEBUG`             - Debug-level on stdout (default: `false`) : `-e LEVEL_DEBUG="<true|false>"`
* `LEVEL_INFO`              - Info-level on stdout (default: `false`) : `-e LEVEL_INFO="<true|false>"`

### Volumes

* `/var/lib/synsec/data/` - Directory where all synsec data (Databases) is located

* `/etc/synsec/` - Directory where all synsec configurations are located

#### Useful File Locations

* `/usr/local/bin/synsec` - Synsec binary
  
* `/usr/local/bin/ccscli` - Synsec CLI binary to interact with synsec

## Find Us

* [GitHub](https://bitbucket.org/Aishee/synsec)

## Contributing

Please read [contributing](https://docs.synsec.net/Synsec/v1/contributing/) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE](https://bitbucket.org/Aishee/synsec/blob/master/LICENSE) file for details.
