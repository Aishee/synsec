package csconfig

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

/*Configurations needed for synsec to load parser/scenarios/... + acquisition*/
type SynsecServiceCfg struct {
	AcquisitionFilePath string `yaml:"acquisition_path,omitempty"`
	AcquisitionDirPath  string `yaml:"acquisition_dir,omitempty"`

	AcquisitionFiles     []string          `yaml:"-"`
	ParserRoutinesCount  int               `yaml:"parser_routines"`
	BucketsRoutinesCount int               `yaml:"buckets_routines"`
	OutputRoutinesCount  int               `yaml:"output_routines"`
	SimulationConfig     *SimulationConfig `yaml:"-"`
	LintOnly             bool              `yaml:"-"`                          //if set to true, exit after loading configs
	BucketStateFile      string            `yaml:"state_input_file,omitempty"` //if we need to unserialize buckets at start
	BucketStateDumpDir   string            `yaml:"state_output_dir,omitempty"` //if we need to unserialize buckets on shutdown
	BucketsGCEnabled     bool              `yaml:"-"`                          //we need to garbage collect buckets when in forensic mode

	HubDir             string `yaml:"-"`
	DataDir            string `yaml:"-"`
	ConfigDir          string `yaml:"-"`
	HubIndexFile       string `yaml:"-"`
	SimulationFilePath string `yaml:"-"`
}

func (c *Config) LoadSynsec() error {
	var err error
	// Configuration paths are dependency to load synsec configuration
	if err := c.LoadConfigurationPaths(); err != nil {
		return err
	}

	if c.Synsec == nil {
		log.Warningf("synsec agent is disabled")
		c.DisableAgent = true
		return nil
	}
	if c.Synsec.AcquisitionFilePath != "" {
		log.Debugf("non-empty acquisition file path %s", c.Synsec.AcquisitionFilePath)
		if _, err := os.Stat(c.Synsec.AcquisitionFilePath); err != nil {
			return errors.Wrapf(err, "while checking acquisition path %s", c.Synsec.AcquisitionFilePath)
		}
		c.Synsec.AcquisitionFiles = append(c.Synsec.AcquisitionFiles, c.Synsec.AcquisitionFilePath)
	}
	if c.Synsec.AcquisitionDirPath != "" {
		c.Synsec.AcquisitionDirPath, err = filepath.Abs(c.Synsec.AcquisitionDirPath)
		if err != nil {
			return errors.Wrapf(err, "can't get absolute path of '%s'", c.Synsec.AcquisitionDirPath)
		}
		files, err := filepath.Glob(c.Synsec.AcquisitionDirPath + "/*.yaml")
		if err != nil {
			return errors.Wrap(err, "while globing acquis_dir")
		}
		c.Synsec.AcquisitionFiles = append(c.Synsec.AcquisitionFiles, files...)
	}
	if c.Synsec.AcquisitionDirPath == "" && c.Synsec.AcquisitionFilePath == "" {
		return fmt.Errorf("no acquisition_path nor acquisition_dir")
	}

	c.Synsec.ConfigDir = c.ConfigPaths.ConfigDir
	c.Synsec.DataDir = c.ConfigPaths.DataDir
	c.Synsec.HubDir = c.ConfigPaths.HubDir
	c.Synsec.HubIndexFile = c.ConfigPaths.HubIndexFile
	if c.Synsec.ParserRoutinesCount <= 0 {
		c.Synsec.ParserRoutinesCount = 1
	}

	if c.Synsec.BucketsRoutinesCount <= 0 {
		c.Synsec.BucketsRoutinesCount = 1
	}

	if c.Synsec.OutputRoutinesCount <= 0 {
		c.Synsec.OutputRoutinesCount = 1
	}

	var synsecCleanup = []*string{
		&c.Synsec.AcquisitionFilePath,
	}
	for _, k := range synsecCleanup {
		if *k == "" {
			continue
		}
		*k, err = filepath.Abs(*k)
		if err != nil {
			return errors.Wrapf(err, "failed to get absolute path of '%s'", *k)
		}
	}
	for i, file := range c.Synsec.AcquisitionFiles {
		f, err := filepath.Abs(file)
		if err != nil {
			return errors.Wrapf(err, "failed to get absolute path of '%s'", file)
		}
		c.Synsec.AcquisitionFiles[i] = f
	}

	if err := c.LoadAPIClient(); err != nil {
		return fmt.Errorf("loading api client: %s", err.Error())
	}
	if err := c.LoadHub(); err != nil {
		return fmt.Errorf("loading hub: %s", err)
	}
	return nil
}
